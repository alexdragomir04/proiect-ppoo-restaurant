
DATA: 2020-10-15
-------------------------
Numar comenzi nepreluate: 0
Numar comenzi preluate: 2
========================== RAPORT COMENZI INCHEIATE ==========================
TOTAL INCASARI ZILNICE: 60 lei
TOTAL PRODUSE VANDUTE: 6
TOTAL TIMP PETRECUT: 60 minute
TOTAL COMENZI PLATITE CU CARDUL/CASH: 0/2

DATA: 2020-10-31
-------------------------
Numar comenzi nepreluate: 0
Numar comenzi preluate: 1
========================== RAPORT COMENZI INCHEIATE ==========================
TOTAL INCASARI ZILNICE: 24 lei
TOTAL PRODUSE VANDUTE: 2
TOTAL TIMP PETRECUT: 10 minute
TOTAL COMENZI PLATITE CU CARDUL/CASH: 1/0

DATA: 2020-11-04
-------------------------
Numar comenzi nepreluate: 0
Numar comenzi preluate: 2
========================== RAPORT COMENZI INCHEIATE ==========================
TOTAL INCASARI ZILNICE: 86 lei
TOTAL PRODUSE VANDUTE: 6
TOTAL TIMP PETRECUT: 40 minute
TOTAL COMENZI PLATITE CU CARDUL/CASH: 1/1
