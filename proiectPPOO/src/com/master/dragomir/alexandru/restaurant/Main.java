package com.master.dragomir.alexandru.restaurant;

import com.master.dragomir.alexandru.restaurant.models.*;
import com.master.dragomir.alexandru.restaurant.models.enums.ClassType;
import com.master.dragomir.alexandru.restaurant.models.enums.TipPlata;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Restaurant restaurant = Restaurant.getInstance("AuraPlace");
        try {
            restaurant.afiseazaMeniu();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}

