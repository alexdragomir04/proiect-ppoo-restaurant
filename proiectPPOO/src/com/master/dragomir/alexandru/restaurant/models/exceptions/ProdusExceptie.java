package com.master.dragomir.alexandru.restaurant.models.exceptions;

public class ProdusExceptie extends Exception{

    public ProdusExceptie() {
        super();
    }

    public ProdusExceptie(String message) {
        super(message);
    }

}
