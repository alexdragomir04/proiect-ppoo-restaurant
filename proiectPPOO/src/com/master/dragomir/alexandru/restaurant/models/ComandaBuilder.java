package com.master.dragomir.alexandru.restaurant.models;

import com.master.dragomir.alexandru.restaurant.models.enums.TipPlata;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ComandaBuilder {

    private int id;
    private List<Produs> produse;
    private String timpEstimat;
    private TipPlata tipPlata;
    private LocalDate dataComanda;
    private boolean ready;
    private Float pretTotal;

    public ComandaBuilder(int id) {
        this.id=id;
        this.produse = new ArrayList<>();
        this.timpEstimat = "0";
        this.tipPlata = null;
        this.dataComanda = LocalDate.now();
        this.ready = false;
        this.pretTotal = 0.0f;
        this.ready = false;
    }

    public ComandaBuilder setProduse(List<Produs> produse) {
        this.produse = produse;
        return this;
    }

    public ComandaBuilder setTimpEstimat(String timpEstimat) {
        this.timpEstimat = timpEstimat;
        return this;
    }

    public ComandaBuilder setTipPlata(TipPlata tipPlata) {
        this.tipPlata = tipPlata;
        return this;
    }

    public ComandaBuilder setPretTotal(Float pretTotal) {
        this.pretTotal = pretTotal;
        return this;
    }

    public List<Produs> getProduse() {
        return produse;
    }

    public ComandaBuilder isReady(){
        this.ready=true;
        return this;
    }

    public ComandaBuilder setData(LocalDate data){
        this.dataComanda=data;
        return this;
    }

    public Comanda build() {
        return new Comanda(this.id,this.produse, this.timpEstimat, pretTotal, this.tipPlata,ready, this.dataComanda);
    }

    public String arataComanda(List<Produs> produse,StringBuilder builder){
        builder.delete(0,builder.length());
        for (Produs produs : produse) {
            builder.append(produs.toComandaMeniuString()).append("\n");
        }
        builder.append("\nTIMP ESTIMAT: ").append(timpEstimat).append("ute\nPRET TOTAL: ").append(pretTotal).append(" lei")
                .append("\nTIP PLATA: ").append(tipPlata.name())
        .append("\nDaca comanda este in regula apasati Y. Pentru anulare apasati orice alta tasta!");
        return builder.toString();
    }

}
