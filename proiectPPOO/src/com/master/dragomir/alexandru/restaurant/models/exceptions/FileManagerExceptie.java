package com.master.dragomir.alexandru.restaurant.models.exceptions;

public class FileManagerExceptie extends Exception {

    public FileManagerExceptie() {
        super();
    }

    public FileManagerExceptie(String message) {
        super(message);
    }

}
