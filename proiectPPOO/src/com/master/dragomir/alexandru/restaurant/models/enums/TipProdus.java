package com.master.dragomir.alexandru.restaurant.models.enums;

public enum TipProdus {
    CIORBA,
    BURGERS,
    SALATA,
    BAUTURA_ALCOOLICA,
    BAUTURA_NONALCOOLICA,
}
