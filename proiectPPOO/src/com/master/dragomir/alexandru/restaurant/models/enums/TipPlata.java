package com.master.dragomir.alexandru.restaurant.models.enums;

public enum TipPlata {
    CASH,
    CARD
}
