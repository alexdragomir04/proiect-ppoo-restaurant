package com.master.dragomir.alexandru.restaurant.models;

import com.master.dragomir.alexandru.restaurant.models.enums.TipProdus;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Produs implements Serializable {

    private final long id;
    private final TipProdus categorie;
    private final String numeProdus;
    private final String timpEstimatServire;
    private final String[] continut;
    private final String[][] valoriNutritive;
    private final String cantitate;
    private final Float pret;

    public Produs(long id, TipProdus categorie, String numeProdus, String timpEstimatServire, String[] continut, String[][] valoriNutritive, String cantitate, Float pret) {
        this.id = id;
        this.categorie = categorie;
        this.numeProdus = numeProdus;
        this.timpEstimatServire = timpEstimatServire;
        this.continut = continut;
        this.valoriNutritive = valoriNutritive;
        this.cantitate = cantitate;
        this.pret = pret;
    }

    public static Produs parseObjectFromString(String[] s) {
        try {
            return new Produs(Integer.parseInt(s[0]), null, s[1], s[2], null, null, s[3], Float.parseFloat(s[4]));
        } catch (Exception ex) {
            System.out.println("Exceptie la transformarea unui produs!");
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produs produs = (Produs) o;
        return categorie == produs.categorie &&
                id == produs.id &&
                numeProdus.equals(produs.numeProdus) &&
                timpEstimatServire.equals(produs.timpEstimatServire) &&
                Arrays.equals(continut, produs.continut) &&
                Arrays.equals(valoriNutritive, produs.valoriNutritive) &&
                cantitate.equals(produs.cantitate) &&
                pret.equals(produs.pret);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(categorie, numeProdus, timpEstimatServire, cantitate, pret);
        result = 31 * result + Arrays.hashCode(continut);
        result = 31 * result + Arrays.hashCode(valoriNutritive);
        return result;
    }

    public Float getPret() {
        return pret;
    }

    public long getId() {
        return id;
    }

    public TipProdus getCategorie() {
        return categorie;
    }

    public String getNumeProdus() {
        return numeProdus;
    }

    public String getCantitate() {
        return cantitate;
    }

    public String toComandaString() {
        return id + "," + numeProdus + "," + timpEstimatServire + "," + cantitate +"," + pret + "f";
    }

    public String toComandaMeniuString() {
        return id + "." + numeProdus + " | " + cantitate +" | " + pret + " lei";
    }

    public String getTimpEstimatServire() {
        return timpEstimatServire;
    }

    public String toMeniuString(StringBuilder builder) {
        builder.delete(0, builder.length());
        builder.append(id).append(".").append(" ").append(numeProdus).append(" | Cantitate: ").append(cantitate).append(" | Pret: ").append(pret).append(" lei\n");

        if (!categorie.equals(TipProdus.BAUTURA_ALCOOLICA) && !categorie.equals(TipProdus.BAUTURA_NONALCOOLICA)) {
            builder.append("\n Continut: ");
            for (String s : continut) builder.append(s).append(" | ");
            builder.append("\n Valori nutritive: ");
            for (String[] strings : valoriNutritive)
                for (String string : strings) {
                    builder.append(string).append(" ");
                }
            builder.append("\n");
        }
        builder.append("Timp aproximativ de servire: ").append(timpEstimatServire).append("\n");
        return builder.toString();
    }

}
