package com.master.dragomir.alexandru.restaurant.models;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class Restaurant {

    private static String numeRestaurant;
    private static Restaurant restaurant = null;
    private Meniu meniu;
    Map<Integer, Comanda> comenzi;

    private Restaurant(String numeRestaurant) {
        Restaurant.numeRestaurant = numeRestaurant;
        this.meniu = new Meniu();
        this.comenzi = new HashMap<>();
    }


    public static Restaurant getInstance(String numeRestaurant) {
        if (restaurant == null) {
            restaurant = new Restaurant(numeRestaurant);
        }
        return restaurant;
    }

    public void afiseazaMeniu() throws FileNotFoundException, ParseException {
        meniu.start(numeRestaurant);
    }

}
