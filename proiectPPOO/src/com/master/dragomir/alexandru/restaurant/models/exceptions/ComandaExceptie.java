package com.master.dragomir.alexandru.restaurant.models.exceptions;

public class ComandaExceptie extends Exception{


    public ComandaExceptie() {
        super();
    }

    public ComandaExceptie(String message) {
        super(message);
    }

}
