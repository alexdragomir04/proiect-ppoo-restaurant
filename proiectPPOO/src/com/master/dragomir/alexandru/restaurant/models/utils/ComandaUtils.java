package com.master.dragomir.alexandru.restaurant.models.utils;

import com.master.dragomir.alexandru.restaurant.models.Comanda;
import com.master.dragomir.alexandru.restaurant.models.ComandaBuilder;
import com.master.dragomir.alexandru.restaurant.models.Produs;
import com.master.dragomir.alexandru.restaurant.models.enums.TipPlata;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ComandaUtils {

    public static String toMeniuComanda(Comanda c, StringBuilder builder) {
        builder.delete(0, builder.length());
        builder.append("Comanda cu ID-ul: ").append(c.getIdComanda());
        builder.append("\n======================\n");
        builder.append("\nData comanda: ").append(c.getDataComanda());
        builder.append("\nProduse: \n");
        c.getProduse()
                .forEach(produs -> builder.append(produs.getNumeProdus()).append(" | "));
        builder.append("\n");
        c.getProduse()
                .forEach(produs -> builder.append(produs.getCantitate()).append(" ").append(produs.getPret()).append("lei | "));
        builder.append("\nTimp estimat: ").append(c.getTimpEstimat()).append(" | ").append(" Pret total: ").append(calculeazaPretTotal(c)).append("lei");
        builder.append("\nStatus: ");
        if (c.isReady()) builder.append("Finalizata\n");
        else builder.append("In pregatire\n");
        return builder.toString();
    }


    public static Float calculeazaPretTotal(Comanda c) {
        AtomicReference<Float> pretTotal = new AtomicReference<>(0.0f);
        c.getProduse()
                .forEach(produs -> pretTotal.updateAndGet(v -> v + produs.getPret()));
        return pretTotal.get();
    }

    public static Float calculeazaPretTotal(ComandaBuilder builder) {
        AtomicReference<Float> pretTotal = new AtomicReference<>(0.0f);
        builder.getProduse()
                .forEach(produs -> pretTotal.updateAndGet(v -> v + produs.getPret()));
        return pretTotal.get();
    }

    public static Integer calculeazaTimpTotalEstimat(ComandaBuilder builder) {
        AtomicReference<Integer> timpTotalEstimat = new AtomicReference<>(0);
        builder.getProduse()
                .forEach(produs -> timpTotalEstimat.updateAndGet(v -> v + Integer.parseInt(produs.getTimpEstimatServire().split(" ")[0])));
        return timpTotalEstimat.get();
    }


    public static String writeToCsv(Comanda c) {
        StringBuilder builder = new StringBuilder();
        builder.append(c.getIdComanda()).append(";");
        List<Produs> tempProduse = c.getProduse();
        AtomicReference<Integer> indexProdus = new AtomicReference<>(1);
        tempProduse.forEach(produs -> {
            if (tempProduse.size() != indexProdus.get()) {
                builder.append(produs.toComandaString()).append("/");
                indexProdus.updateAndGet(v -> v + 1);
            } else builder.append(produs.toComandaString());
        });
        if (c.isReady()) builder.append(";Finalizata");
        else builder.append(";In pregatire");
        c.setPretTotal(ComandaUtils.calculeazaPretTotal(c));
        builder.append(";").append(c.getTimpEstimat()).append(";").append(c.getPretTotal()).append("f;").append(c.getTipPlata().name()).append(";").append(c.getDataComanda().toString()).append(";\n");
        return builder.toString();
    }

    public static void incheieSiAfiseazaComanda(Comanda c, StringBuilder builder) {
        c.setReady(true);
        System.out.println(toMeniuComanda(c, builder));
    }

    private static Float calculeazaPretMediuTotal(List<Comanda> listaComenzi) {
        AtomicReference<Float> pretMediu = new AtomicReference<>(0.0f);
        if (listaComenzi.size() != 0) {
            listaComenzi
                    .forEach(comanda -> pretMediu.updateAndGet(c -> c + comanda.getPretTotal()));
        }
        if (pretMediu.get() > 0)
            return pretMediu.get() / listaComenzi.size();
        else return 0.0f;
    }

    private static Float calculeazaNumarMediuComenziPeZi(List<Comanda> listaComenzi) {
        AtomicReference<Float> numarMediuComenzi = new AtomicReference<>(0.0f);
        HashMap<LocalDate, List<Comanda>> dateComenziHash = preiaDateDespreComenziPeZile(listaComenzi);
        dateComenziHash.forEach((localDate, comenzi) -> numarMediuComenzi.updateAndGet(v -> v + comenzi.size()));
        if (numarMediuComenzi.get() > 0) {
            return numarMediuComenzi.get() / dateComenziHash.size();
        }
        return 0.0f;
    }

    private static HashMap<LocalDate, List<Comanda>> preiaDateDespreComenziPeZile(List<Comanda> listaComenzi) {
        HashMap<LocalDate, List<Comanda>> dateComenziHash = new HashMap<>();
        if (listaComenzi.size() != 0) {
            listaComenzi
                    .forEach(comanda -> {
                        if (dateComenziHash.get(comanda.getDataComanda()) == null) {
                            List<Comanda> comenziDataSpecifica = new ArrayList<>();
                            comenziDataSpecifica.add(comanda);
                            dateComenziHash.put(comanda.getDataComanda(), comenziDataSpecifica);
                        } else {
                            dateComenziHash.get(comanda.getDataComanda()).add(comanda);
                        }
                    });
        }
        return dateComenziHash;
    }

    private static Float calculeazaTimpEstimatMediu(List<Comanda> listaComenzi) {
        AtomicReference<Float> timpEstimatMediu = new AtomicReference<>(0.0f);
        if (listaComenzi.size() != 0) {
            listaComenzi
                    .forEach(comanda -> timpEstimatMediu.updateAndGet(c -> c + Float.parseFloat(comanda.getTimpEstimat().split(" ")[0])));
        }
        if (timpEstimatMediu.get() > 0)
            return timpEstimatMediu.get() / listaComenzi.size();
        else return 0.0f;
    }

    private static HashMap<TipPlata, Float> calculeazaPondereMedieTipPlata(List<Comanda> listaComenzi) {
        HashMap<TipPlata, Float> numarMediuTipPlata = new HashMap<>();
        Arrays
                .stream(TipPlata.values())
                .forEach(tipPlata -> {
                    long numarComenziTipPlata = listaComenzi.stream()
                            .filter(comanda -> comanda.getTipPlata() == tipPlata)
                            .count();
                    numarMediuTipPlata.put(tipPlata, (numarComenziTipPlata / (float) listaComenzi.size()));
                });
        return numarMediuTipPlata;
    }

    private static Map<String, Integer> calculeazaCeleMaiVanduteProduse(List<Comanda> comenzi) {
        TreeMap<String, Integer> idProdusSiNumar = new TreeMap<>();
        comenzi.
                forEach(comanda -> {
                    List<Produs> produse = comanda.getProduse();
                    produse.forEach(produs -> idProdusSiNumar.merge(produs.getNumeProdus().toLowerCase(), 1, Integer::sum));
                });
        return sortByValues(idProdusSiNumar);
    }


    public static String genereazaRaportStatisticiZilnice(List<Comanda> comenzi, StringBuilder builder) {
        builder.delete(0, builder.length());
        HashMap<LocalDate, List<Comanda>> comenziPeZile = preiaDateDespreComenziPeZile(comenzi);

        comenziPeZile.forEach((data, comenziPeData) -> {
            List<Comanda> comenziIncheiate = comenziPeData
                    .stream()
                    .filter(Comanda::isReady)
                    .collect(Collectors.toList());
            Long numarComenziNepreluate = comenziPeData
                    .stream()
                    .filter(comanda -> !comanda.isReady())
                    .count();
            Double totalIncasariZilnice = comenziIncheiate
                    .stream()
                    .mapToDouble(ComandaUtils::calculeazaPretTotal)
                    .sum();
            Integer totalProduseVandute = comenziIncheiate
                    .stream()
                    .map(Comanda::getProduse)
                    .mapToInt(List::size)
                    .sum();
            Integer totalTimpPetrecut = comenziIncheiate
                    .stream()
                    .map(Comanda::getTimpEstimat)
                    .mapToInt(timpPerComanda -> Integer.parseInt(timpPerComanda.split(" ")[0]))
                    .sum();
            Integer totalComenziCard = (int) comenziIncheiate
                    .stream()
                    .filter(comanda -> comanda.getTipPlata() == TipPlata.CARD)
                    .count();
            Integer totalComenziCash = (int) comenziIncheiate
                    .stream()
                    .filter(comanda -> comanda.getTipPlata() == TipPlata.CASH)
                    .count();

            builder.append(MessageFormat.format("""
                                                        
                            DATA: {0}
                            -------------------------
                            Numar comenzi nepreluate: {1}
                            Numar comenzi preluate: {2}
                            ========================== RAPORT COMENZI INCHEIATE ==========================
                            TOTAL INCASARI ZILNICE: {3} lei
                            TOTAL PRODUSE VANDUTE: {4}
                            TOTAL TIMP PETRECUT: {5} minute
                            TOTAL COMENZI PLATITE CU CARDUL/CASH: {6}/{7}
                            """, data, numarComenziNepreluate, comenziIncheiate.size(), totalIncasariZilnice, totalProduseVandute, totalTimpPetrecut, totalComenziCard
                    , totalComenziCash));
        });

        return builder.toString();
    }

    //scriere/citire fisier 4 metode statistici+rapoarte

    public static String calculeazaStatistici(List<Comanda> listaComenzi, StringBuilder builder) {
        builder.delete(0, builder.length());
        DecimalFormat df = new DecimalFormat("0.0");
        Map<String, Integer> celeMaiVanduteProduseHash = calculeazaCeleMaiVanduteProduse(listaComenzi);
        StringBuilder produseVandute = new StringBuilder();
        int index = 0;
        for (Map.Entry<String, Integer> entry : celeMaiVanduteProduseHash.entrySet()) {
            produseVandute.append((entry.getKey() + " (" + entry.getValue() + ") | ").toUpperCase());
            index++;
            if (index == 5) break;
        }
        return builder.append("========================== STATISTICI COMENZI ========================== \n" + "TIMP MEDIU ESTIMAT: ")
                .append(df.format(calculeazaTimpEstimatMediu(listaComenzi)))
                .append(" minute")
                .append("\nPRET MEDIU ESTIMAT: ")
                .append(df.format(calculeazaPretMediuTotal(listaComenzi)))
                .append(" lei").append("\nNUMAR MEDIU DE COMENZI PE ZI: ")
                .append(df.format(calculeazaNumarMediuComenziPeZi(listaComenzi)))
                .append("\nTIP PLATA: CARD ")
                .append(df.format(calculeazaPondereMedieTipPlata(listaComenzi).get(TipPlata.CARD) * 100))
                .append("% | CASH ")
                .append(df.format(calculeazaPondereMedieTipPlata(listaComenzi).get(TipPlata.CASH) * 100))
                .append("%").append("\nCELE MAI VANDUTE PRODUSE: ")
                .append(produseVandute)
                .toString();

    }

    public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator = (k1, k2) -> {
            int compare = map.get(k2).compareTo(map.get(k1));
            if (compare == 0) return -1;
            return compare;
        };
        Map<K, V> sortedByValues = new TreeMap<>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }

}
