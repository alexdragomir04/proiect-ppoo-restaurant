package com.master.dragomir.alexandru.restaurant.models.utils;

import com.master.dragomir.alexandru.restaurant.models.Comanda;
import com.master.dragomir.alexandru.restaurant.models.Produs;
import com.master.dragomir.alexandru.restaurant.models.enums.ClassType;
import com.master.dragomir.alexandru.restaurant.models.enums.TipPlata;
import com.master.dragomir.alexandru.restaurant.models.enums.TipProdus;
import com.master.dragomir.alexandru.restaurant.models.exceptions.FileManagerExceptie;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtils<T> {

    private static BufferedReader in;
    private static BufferedWriter out;
    private List<T> list;

    public static void writeStringToFile(FileWriter file, String string) {
        try {
            out = new BufferedWriter(file);
            out.flush();
            out.write(string);
            out.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<T> readFromFile(FileReader file, ClassType classType) {
        try {
            in = new BufferedReader(file);
            if (classType == ClassType.PRODUS) {
                list = (List<T>) in
                        .lines()
                        .skip(1)
                        .map(line -> {
                            String[] produsCsv = line.split(";");
                            int idProdus = Integer.parseInt(produsCsv[0]);
                            String numeProdus = produsCsv[1];
                            TipProdus categorie = switch (produsCsv[2]) {
                                case "SALATA" -> TipProdus.SALATA;
                                case "BURGERS" -> TipProdus.BURGERS;
                                case "CIORBA" -> TipProdus.CIORBA;
                                case "BAUTURA_ALCOOLICA" -> TipProdus.BAUTURA_ALCOOLICA;
                                case "BAUTURA_NONALCOOLICA" -> TipProdus.BAUTURA_NONALCOOLICA;
                                default -> null;
                            };
                            String timpEstimat = produsCsv[3];
                            String cantitate = produsCsv[4];
                            Float pret = Float.parseFloat(produsCsv[5]);
                            String[] produsCsvContinut = produsCsv[6].split(",");
                            String[] valoriNutritiveCsv = produsCsv[7].split("/");
                            String[][] valoriMatrice = new String[4][2];
                            int i = 0, j = 0;
                            for (String valoareNutritiva : valoriNutritiveCsv) {
                                String[] valoriSplit = valoareNutritiva.split(":");
                                valoriMatrice[i][j] = valoriSplit[0];
                                j++;
                                valoriMatrice[i][j] = valoriSplit[1];
                                i++;
                                j = 0;
                            }
                            return new Produs(idProdus, categorie, numeProdus, timpEstimat, produsCsvContinut, valoriMatrice, cantitate, pret);
                        })
                        .collect(Collectors.toList());
                return list;

            } else if (classType == ClassType.COMANDA) {
                list = (List<T>) in
                        .lines()
                        .skip(1)
                        .map(line -> {

                            String[] comandaCsv = line.split(";");
                            int idComanda = Integer.parseInt(comandaCsv[0]);
                            String[] produseComandaCsv = comandaCsv[1].split("/");
                            List<Produs> produse = new ArrayList<>();
                            Arrays.stream(produseComandaCsv).forEach(obj -> {
                                produse.add(Produs.parseObjectFromString(obj.split(",")));
                            });
                            boolean ready = false;
                            if (!comandaCsv[2].equals("In pregatire")) ready = true;
                            String timpEstimat = comandaCsv[3];
                            Float pretTotal = Float.parseFloat(comandaCsv[4]);
                            TipPlata tipPlata = switch (comandaCsv[5]) {
                                case "CASH" -> TipPlata.CASH;
                                case "CARD" -> TipPlata.CARD;
                                default -> null;
                            };
                            LocalDate dataComanda = LocalDate.parse(comandaCsv[6], DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                            return new Comanda(idComanda, produse, timpEstimat, pretTotal, tipPlata, ready, dataComanda);
                        })
                        .collect(Collectors.toList());
                return list;
            } else throw new FileManagerExceptie("Obiectul trebuie sa fie de tip produs sau de tip comanda!");

        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.getMessage());
        }
        return list;
    }

    public void writeToFile(FileWriter file, List<T> list) {
        try {
            out = new BufferedWriter(file);
            List<Comanda> listaComenzi = (List<Comanda>) list;
            out.flush();
            out.write("id_comanda;produse;status;timp_estimat;pret_total;tip_plata;data_comanda;");
            out.newLine();
            listaComenzi.forEach(obj -> {
                try {
                    out.write(ComandaUtils.writeToCsv(obj));
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            });
            out.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " " + ex.fillInStackTrace());
        }
    }


}
