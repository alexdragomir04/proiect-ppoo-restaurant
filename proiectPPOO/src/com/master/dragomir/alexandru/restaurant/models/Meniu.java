package com.master.dragomir.alexandru.restaurant.models;

import com.master.dragomir.alexandru.restaurant.models.enums.ClassType;
import com.master.dragomir.alexandru.restaurant.models.enums.TipPlata;
import com.master.dragomir.alexandru.restaurant.models.enums.TipProdus;
import com.master.dragomir.alexandru.restaurant.models.exceptions.ProdusExceptie;
import com.master.dragomir.alexandru.restaurant.models.utils.ComandaUtils;
import com.master.dragomir.alexandru.restaurant.models.utils.FileUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Meniu {

    private static FileUtils<Produs> fileProduse;
    private static FileUtils<Comanda> fileComenzi;
    private static List<Produs> listaProduse;
    private static List<Comanda> listaComenzi;
    private static Scanner scanner;
    private static ComandaBuilder builder;
    private static StringBuilder stringBuilder;
    private static String raport;
    private static String statistics;

    public Meniu() {
        fileProduse = new FileUtils<>();
        fileComenzi = new FileUtils<>();
        listaProduse = new ArrayList<Produs>();
        listaComenzi = new ArrayList<>();
        scanner = new Scanner(System.in);
        stringBuilder = new StringBuilder();
        raport=null;
        statistics = null;
    }

    public void start(String nume_restaurant) throws FileNotFoundException, ParseException {
        listaProduse = fileProduse.readFromFile(new FileReader("src/com/master/dragomir/alexandru/restaurant/files/produse.csv"),
                ClassType.PRODUS);
        listaComenzi = fileComenzi.readFromFile(new FileReader("src/com/master/dragomir/alexandru/restaurant/files/comenzi.csv"),
                ClassType.COMANDA);

        int menuOption = -1;
        do {
            stringBuilder.append("Bun venit la restaurantul ").append(nume_restaurant).append("! \n\n Va rugam sa alegeti o optiune:\n");
            stringBuilder.append("""
                    1. Vizualizati produse\s
                    2. Adaugati o comanda\s
                    3. Vizualizati comenzi\s
                    4. Incheie o comanda\s
                    5. Vizualizati statistici despre comenzi
                    6. Genereaza rapoarte zilnice\s
                    7. Inchide meniul\s

                    Va rugam sa alegeti o optiune (ex: 3):""");
            System.out.println(stringBuilder.toString());

            String option = "";
            do {
                try {
                    option = scanner.next();
                    if (!option.matches("^([1-7])$"))
                        throw new ParseException("Trebuie sa fie o optiuna viabila!", 0);
                    menuOption = Integer.parseInt(option);
                } catch (ParseException ex) {
                    System.out.println(ex.getMessage() + "\n");
                }
            } while (!option.matches("^([1-7])$"));

            switch (menuOption) {
                case 1: {
                    System.out.println("============================= PRODUSE =============================\n");
                    Arrays.stream(TipProdus
                            .values())
                            .forEach(tipProdus -> {
                                System.out.println("\nCategorie: " + tipProdus.name());
                                for (int i = 0; i < listaProduse.size(); i++) {
                                    if (listaProduse.get(i).getCategorie().equals(tipProdus)) {
                                        System.out.println(listaProduse.get(i).toMeniuString(stringBuilder));
                                    }
                                }
                            });
                    System.out.println("\n===================================================================\n");
                    System.out.println("\nApasati orice cifra pentru a reveni la meniul principal!");
                    scanner.next();
                    menuOption = 1;
                    break;
                }
                case 2: {
                    System.out.println("""
                            Va rugam sa alegeti produsele pe care le doriti introducand id-ul( ex 1,5,8):\s
                             Pentru a anula apasati 0!
                             Pentru a vizualiza produsele introduceti cuvantul `produse`""");
                    String inputString = "";
                    boolean isValid = false;
                    do {
                        try {
                            inputString = scanner.next();
                            if (inputString.equals("produse")) {
                                listaProduse.stream().map(Produs::toComandaMeniuString).forEach(System.out::println);
                                System.out.println("\n Introduceti id-urile produsulelor separate de virgula (ex: 1,5,8): ");
                                inputString = scanner.next();
                            }
                            if (inputString.equals("0")) break;
                            if (!inputString.matches("^\\d+(,\\d+)*$"))
                                throw new ParseException("Trebuie sa introduceti id-urile comenzilor separate de ','!", 0);

                            String[] produse = inputString.trim().split(",");
                            List<Produs> tempProduse = new ArrayList<>();
                            for (String produs : produse) {
                                tempProduse.add(listaProduse.stream()
                                        .filter(produsLista -> produsLista.getId() == Integer.parseInt(produs))
                                        .findFirst()
                                        .orElseThrow(ProdusExceptie::new));
                            }
                            int lastId = (int) listaComenzi.stream()
                                    .map(Comanda::getID_COMANDA)
                                    .count();
                            builder = new ComandaBuilder(lastId + 1);
                            System.out.println("Introduceti modul de plata (cash/card)");
                            String tipPlata = "";
                            do {
                                tipPlata = scanner.next();
                                if (!tipPlata.toUpperCase().equals("CASH") && !tipPlata.toUpperCase().equals("CARD")) {
                                    System.out.println("Optiunea nu exista, va rugam reintroduceti!");
                                }
                            } while (!tipPlata.toUpperCase().equals("CASH") && !tipPlata.toUpperCase().equals("CARD"));
                            builder.setProduse(tempProduse);
                            builder.setData(LocalDate.now()).setPretTotal(ComandaUtils.calculeazaPretTotal(builder))
                                    .setTimpEstimat(ComandaUtils.calculeazaTimpTotalEstimat(builder) + " min")
                                    .setTipPlata(TipPlata.valueOf(tipPlata.toUpperCase().trim()));
                            System.out.println(builder.arataComanda(builder.getProduse(), stringBuilder));
                            if (scanner.next().trim().toUpperCase().equals("Y")) {
                                listaComenzi.add(builder.build());
                                System.out.println("Ati creat o comanda! Pentru a o vizualiza apasati optiunea 3 in meniul general!");
                                break;
                            } else break;
                        } catch (ProdusExceptie ex) {
                            System.out.println("Unul din produsele mentionate nu exista! Va rugam reintroduceti!");
                        } catch (ParseException ex) {
                            System.out.println(ex.getMessage());
                        }
                    } while (!inputString.matches("^\\d+(,\\d+)*$") || !inputString.equals("produse"));
                    break;
                }
                case 3: {
                    if (listaComenzi.isEmpty()) System.out.println("Nu exista comenzi!");
                    else {
                        System.out.println("============================= COMENZI =============================\n");
                        ComandaUtils.toMeniuComanda(listaComenzi.get(0), stringBuilder);
                        listaComenzi
                                .forEach(comanda -> {
                                    System.out.println(ComandaUtils.toMeniuComanda(comanda, stringBuilder));
                                });
                        System.out.println("\n===================================================================\n");
                    }
                    System.out.println("\nApasati orice cifra pentru a reveni la meniul principal!");
                    scanner.next();
                    menuOption = 1;
                    break;
                }
                case 4: {
                    System.out.println("Introduceti id-ul comenzii: ");
                    String idComanda = "";
                    do {
                        try {
                            idComanda = "";
                            idComanda = scanner.next();
                            if (!idComanda.matches("^[0-9]*$"))
                                throw new ParseException("Trebuie sa introduceti un numar!", 0);
                            String finalIdComanda = idComanda;
                            Comanda c = listaComenzi.stream()
                                    .filter(comanda -> comanda.getIdComanda() == Integer.parseInt(finalIdComanda))
                                    .findFirst()
                                    .orElse(null);
                            int index = listaComenzi.indexOf(c);
                            if (c != null) {
                                if (!c.isReady()) {
                                    ComandaUtils.incheieSiAfiseazaComanda(c, stringBuilder);
                                    listaComenzi.set(index, c);
                                } else
                                    System.out.println("Comanda cu id-ul specificat nu exista sau este deja finalizata!");
                            }
                        } catch (ParseException ex) {
                            System.out.println(ex.getMessage() + "\n");
                        }
                    } while (!idComanda.matches("^[0-9]*$"));
                    break;
                }
                case 5: {
                    statistics = ComandaUtils.calculeazaStatistici(listaComenzi,stringBuilder);
                    System.out.println(statistics);
                    System.out.println("\n===================================================================\n");
                    System.out.println("Apasati orice cifra pentru a reveni la meniul principal!");
                    scanner.next();
                    menuOption = 1;
                    break;
                }
                case 6: {
                    raport = ComandaUtils.genereazaRaportStatisticiZilnice(listaComenzi,stringBuilder);
                    System.out.println(raport);
                    System.out.println("===================================================================\n");
                    System.out.println("Apasati orice cifra pentru a reveni la meniul principal!");
                    scanner.next();
                    menuOption = 1;
                    break;
                }
                case 7: {
                    try {
                        if(statistics==null)
                        statistics = ComandaUtils.calculeazaStatistici(listaComenzi,stringBuilder);
                        if(raport == null)
                        raport = ComandaUtils.genereazaRaportStatisticiZilnice(listaComenzi,stringBuilder);
                        FileUtils.writeStringToFile(new FileWriter("src/com/master/dragomir/alexandru/restaurant/files/statistici.txt"), statistics);
                        FileUtils.writeStringToFile(new FileWriter("src/com/master/dragomir/alexandru/restaurant/files/raport.txt"), raport);
                        fileComenzi.writeToFile(new FileWriter("src/com/master/dragomir/alexandru/restaurant/files/comenzi.csv",false),listaComenzi);
                        System.out.println("La revedere!");
                        break;
                    }catch(Exception ex){
                        System.out.println(ex.getMessage());
                    }
                }
            }

            stringBuilder.delete(0, stringBuilder.length());
        } while (menuOption < 7);
    }
}
