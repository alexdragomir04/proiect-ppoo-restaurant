package com.master.dragomir.alexandru.restaurant.models;

import com.master.dragomir.alexandru.restaurant.models.enums.TipPlata;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class Comanda implements Serializable {

    private int ID_COMANDA = 0;
    private List<Produs> produse;
    private boolean ready;
    private String timpEstimat;
    private Float pretTotal;
    private TipPlata tipPlata;
    private LocalDate dataComanda;

    public Comanda(int idComanda, List<Produs> produse, String timpEstimat, Float pretTotal, TipPlata tipPlata, boolean ready, LocalDate dataComanda) {
        this.produse = produse;
        this.pretTotal = pretTotal;
        this.ready = false;
        this.timpEstimat = timpEstimat;
        this.tipPlata = tipPlata;
        this.ready = ready;
        this.dataComanda = dataComanda;
        this.ID_COMANDA = idComanda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comanda comanda = (Comanda) o;
        return produse.equals(comanda.produse) &&
                timpEstimat.equals(comanda.timpEstimat) &&
                tipPlata == comanda.tipPlata &&
                dataComanda.equals(comanda.dataComanda);
    }

    @Override
    public int hashCode() {
        return Objects.hash(produse, timpEstimat, tipPlata, dataComanda);
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "produse=" + produse +
                ", timpEsimat=" + timpEstimat +
                ", tipPlata=" + tipPlata +
                ", dataComanda=" + dataComanda +
                ", stare=" + ready +
                ", pretTOtal= " + pretTotal +
                '}';
    }


    public String getTimpEstimat() {
        return timpEstimat;
    }

    public int getID_COMANDA() {
        return ID_COMANDA;
    }

    public Float getPretTotal() {
        return pretTotal;
    }

    public TipPlata getTipPlata() {
        return tipPlata;
    }

    public LocalDate getDataComanda() {
        return dataComanda;
    }

    public List<Produs> getProduse() {
        return produse;
    }

    public int getIdComanda() {
        return ID_COMANDA;
    }

    public boolean isReady() {
        return ready;
    }

    public void ready() {
        this.ready = true;
    }

    public void setID_COMANDA(int ID_COMANDA) {
        this.ID_COMANDA = ID_COMANDA;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public void setPretTotal(Float pretTotal) {
        this.pretTotal = pretTotal;
    }
}
